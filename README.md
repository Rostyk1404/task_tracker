# task_tracker

Project description goes here ...

Getting Started
These instructions will get you a copy of the application up and running on your local machine for development and 
testing purposes. See installing and running the application for detailed notes on how to install and run the application.

Prerequisites
Before running the application make sure that docker and docker-compose are installed on your machine. 
If not follow the docker and docker-compose installation guides.

Installing
To get the working copy of the application clone or download task_tracker repository.

Running the application
Running with docker-compose :

Open the terminal and navigate to task_tracker folder.
Start up the application by running sudo docker-compose up.
Enter http://0.0.0.0:5000/ in a browser to see the application running.
Stop the application, either by running sudo docker-compose down from within your project directory in the second terminal, or by hitting CTRL+C in the original terminal where you started the app.
Note: if you just want to run the application in PyCharm (without docker-compose)

Run the postgres server in docker container by typing:
sudo docker run -d --name container_name -p postrgres_server_port:5432 -p management_plugin_port:5432 postgres:latest
Parameters:

container_name - the name container will get after creation
postgres_server_port - the port on which rabbitmq server will be running
postgres_plugin_port - the port on which postgres will be running
Change HOST = 'localhost' and PORT = 5432 in the user_service/postgres_helper/postgres_config.py

