from app import api
from app.views.tasks import Task, Tasks, GetUsersTasks, GetUsersTask

api.add_resource(Tasks, "/tasks")
api.add_resource(Task, "/tasks/<task_id>")
api.add_resource(GetUsersTasks, "/all_user_tasks/<user_id>")
api.add_resource(GetUsersTask, "/user_tasks/<user_id>/<task_id>")
