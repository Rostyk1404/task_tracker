"""Routes for Users"""

from app import api
from app.views.users import User, Users, Login

api.add_resource(Users, '/users')
api.add_resource(User, '/users/<users_id>')
api.add_resource(Login, '/login')
# api.add_resource(Logout, '/logout')
