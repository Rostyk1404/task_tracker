from flask import request
from flask_restful import Resource
from schema.schemas import TaskSchema
from app.models.tasks import Tasks as DB_TASKS


class Task(Resource):
    def get(self, task_id):
        """
        Method which allow us to get tasks by id
        :param task_id: task_id
        :return: task according to a certain task_id
        """
        task = DB_TASKS.get_task_by_id(task_id)
        task_schema = TaskSchema(only=("task_name", "task_id"))
        output_task = task_schema.dump(task)
        return output_task, 200

    def put(self, task_id):
        """
        Method which allow us to update tasks by id
        :param task_id: task_id
        :return: dict with updated task and status code 200
        """
        body = request.json
        task = DB_TASKS.update_task(task_id, body)
        task_schema = TaskSchema(only=("task_name"))
        output_task = task_schema.dump(task)
        return output_task, 200

    def delete(self, task_id):
        """
        Method which allow us to delete tasks by task id
        :param task_id: task_id
        :return: 204 if task deleted
        """
        DB_TASKS.delete_task(task_id)
        return 204


class Tasks(Resource):
    def get(self):
        """
        Method which allow us to get all tasks
        :return: dict with all tasks
        """
        tasks = DB_TASKS.get_all_tasks()
        task_schema = TaskSchema(only=("task_id", "task_name", "user_id"), many=True)
        output_task = task_schema.dump(tasks)
        return output_task, 200

    def post(self):
        """
        Method which allow us to create tasks
        :return: dict with created task
        """
        body = request.json
        task_name = body.get("task_name")
        user_id = body.get("user_id")
        task = DB_TASKS.create_task(task_name, user_id)
        # task = DB_TASKS.get_task_by_id(task_id)
        task_schema = TaskSchema(only=("task_id", "task_name", "user_id"))
        output_task = task_schema.dump(task)
        return output_task, 200

    def delete(self):
        """
        Method which allow us to delete all tasks
        :return: status code 204 if tasks were deleted successfully
        """
        DB_TASKS.delete_all_tasks()
        return 204


class GetUsersTasks(Resource):
    """
    class GetUsersTasks
    """

    def get(self, user_id):
        """
        Method which allow us to get all users tasks bye user id
        :param user_id:
        :return: list of dicts with all users tasks
        """
        tasks = DB_TASKS.get_all_tasks_for_user(user_id)
        task_schema = TaskSchema(only=("task_name", "user_id"), many=True)
        output_task = task_schema.dump(tasks)
        return output_task, 200


class GetUsersTask(Resource):
    """
    class GetUserTasks
    """

    def get(self, user_id, task_id):
        """
        Method which allow us to get user task bye user id
        :param user_id:
        :param task_id: task id
        :return: list of dicts with all users tasks
        """
        tasks = DB_TASKS.get_task_for_user(user_id, task_id)
        task_schema = TaskSchema(only=("task_name", "user_id"))
        output_task = task_schema.dump(tasks)
        return output_task, 200
