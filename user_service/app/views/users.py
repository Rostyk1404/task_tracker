from datetime import timedelta
from flask import request
from flask_restful import Resource
from flask_jwt_extended import create_access_token
from marshmallow import ValidationError
from app.models.user import (get_user_by_id, update_user, delete_user, get_all_users, registation_user,
                             get_user_by_email, delete_all_users)
from schema.schemas import UserSchema


class User(Resource):
    """
    Class that provide CRUD operation for user
    """

    def get(self, users_id):
        """
        Method which allow us to get user by id
        :param users_id: user id
        :return: user with specific id
        """
        user = get_user_by_id(users_id)
        schema_user = UserSchema(only=("users_id", "email", "username"))
        output_user = schema_user.dump(user)
        return output_user, 200

    def put(self, users_id):
        """
        Method which allow us to update user by id
        :param users_id: user id
        :return: dict with updated user
        """
        body = request.json
        user = update_user(users_id, body)
        schema = UserSchema(only=("email", "username"))
        output_user = schema.dump(user)
        return output_user, 200

    def delete(self, users_id):
        """
        Method which allow us to delete user by id
        :param users_id: user id
        :return: status code 204 if user delete else User does not exist
        """
        return 204 if delete_user(users_id) else 'User does not exist'


class Users(Resource):
    """
    Class that provide CRUD operation for users
    """

    def get(self):
        """
        Method which allow us to get all users
        :return: all users
        """
        users = get_all_users()
        print(users)
        schema = UserSchema(only=("users_id", "email", "username"), many=True)
        print(schema)
        all_users = schema.dump(users)
        return all_users, 200

    def post(self):
        """
        Method which allow us to create users
        :return: dict with created user
        """
        body = request.json
        user_input_schema = UserSchema(only=("email", "password", "username"))
        try:
            input_user = user_input_schema.load(body)
        except ValidationError as e:
            error = str(e)
            return error
        user = registation_user(**input_user)
        user = get_user_by_email(input_user.get("email"))
        user_output_schema = UserSchema(only=("email", "password_hash", "username"))
        output_user = user_output_schema.dump(user)

        return output_user

    def delete(self):
        """
        Method which allow us to delete all users
        :return: status code 204 if users deleted
        """
        delete_all_users()
        return 204


class Login(Resource):
    """
    class Login
    """

    def post(self):
        """
        Method that provides token based authentication
        :return: jwt token if login successfully in other case Invalid email or password
        """
        body = request.json
        email = body.get("email")
        password = body.get("password")
        user = get_user_by_email(email=email)
        if not user.email or not user.check_password(password):
            return 'Invalid email or password', 401

        expires = timedelta(hours=24)
        access_token = create_access_token(identity=user.users_id, expires_delta=expires)
        return access_token, 200
