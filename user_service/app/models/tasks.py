from typing import List
from sqlalchemy import Column, String, Integer, ForeignKey
from sqlalchemy.orm import relationship
from app.database import scoped_session
from app.models import Base


class Tasks(Base):
    """
    Tasks model class
    """
    __tablename__ = 'tasks'
    task_id = Column(Integer, autoincrement=True, primary_key=True)
    task_name = Column(String(180))

    user_id = Column(Integer, ForeignKey("users.users_id", ondelete="CASCADE"))
    user = relationship("Users", backref="tasks")

    @classmethod
    def create_task(cls, task_name: str, user_id: int) -> dict:
        """
        Method that creates tasks
        :param task_name: tasks name
        :param user_id: user id
        :return: dict with created tasks
        """
        with scoped_session() as session:
            tasks = Tasks(task_name=task_name, user_id=user_id)
            session.add(tasks)
            return tasks

    @classmethod
    def get_all_tasks_for_user(cls, user_id: int) -> List[dict]:
        """
        Method that get all tasks for user
        :param user_id: user id
        :return: list of dicts
        """
        with scoped_session() as session:
            return session.query(Tasks).filter_by(user_id=user_id).all()

    @classmethod
    def get_task_for_user(cls, user_id: int, task_id) -> dict:
        """
        Method that get task for user
        :param user_id: user id
        :param task_id: task id
        :return: dict
        """
        with scoped_session() as session:
            return session.query(Tasks).filter_by(user_id=user_id, task_id=task_id).first()

    @classmethod
    def get_task_by_id(cls, task_id: int) -> dict:
        """
        Method that allow us to get task by id
        :param task_id: task id
        :return: task by id
        """
        with scoped_session() as session:
            return session.query(Tasks).filter_by(task_id=task_id).first()

    @classmethod
    def get_task_by_name(cls, task_name: str) -> dict:
        """
        Method that allow us to get task by name
        :param task_name: task name
        :return: task by name
        """
        with scoped_session() as session:
            return session.query(Tasks).filter_by(task_name=task_name).first()

    @classmethod
    def get_all_tasks(cls) -> List[dict]:
        """
        Method that allow us to get all tasks
        :return: all tasks
        """
        with scoped_session() as session:
            return session.query(Tasks).all()

    @classmethod
    def update_task(cls, task_id: int, task_data: dict) -> dict:
        """
        Method that allow us to update task by id
        :param task_id: task_id
        :param task_data: fields that should be updated
        :return: dict with updated tasks
        """
        with scoped_session() as session:
            task = session.query(Tasks).filter_by(task_id=task_id).one()
            for k, v in task_data.items():
                setattr(task, k, v)
            return task

    @classmethod
    def delete_task(cls, task_id: int):
        """
        Method that allow us to delete task by id
        :param task_id: task_id
        :return: deleted task
        """
        with scoped_session() as session:
            return session.query(Tasks).filter_by(task_id=task_id).delete()

    @classmethod
    def delete_all_tasks(cls):
        """
        Method that allow us to delete all task
        :return: deleted task
        """
        with scoped_session() as session:
            return session.query(Tasks).delete()
