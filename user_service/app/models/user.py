from typing import List
from werkzeug.security import generate_password_hash, check_password_hash
from sqlalchemy import Column, String, Integer
from sqlalchemy.orm import relationship
from app.models import Base
from app.database import scoped_session
from app.models.tasks import Tasks


class Users(Base):
    """
    Users model class
    """
    __tablename__ = "users"
    users_id = Column(Integer, autoincrement=True, primary_key=True)
    email = Column(String(50), index=True, unique=True, nullable=False)
    password_hash = Column(String(128))
    username = Column(String(25))
    child = relationship(Tasks, cascade="all,delete", backref="users")

    def set_password(self, password):
        """
        Method that generate password hash
        :param password: password
        :return: set password
        """
        self.password_hash = generate_password_hash(password)

    def check_password(self, password):
        """
        Method checks a password against a given salted and hashed password value
        :param password:
        :return: True if password is the same False if not
        """
        return check_password_hash(self.password_hash, password)

    def __repr__(self):
        return f"<User {self.email}/{self.username}>"


USERS_TYPE = Users


def get_user_by_email(email: str):
    """
    Function that allow to get user by email
    :param email: user email
    :return: user with specific email
    """
    with scoped_session() as session:
        return session.query(Users).filter_by(email=email).first()


def get_user_by_id(users_id: int) -> dict:
    """
    Function that allow to get user by user id
    :param users_id: user id
    :return: user with specific id
    """
    with scoped_session() as session:
        return session.query(Users).filter_by(users_id=users_id).first()


def get_user_by_username(username: str) -> dict:
    """
    Function that allow to get_user_by_username
    :param username: username
    :return: user by specific name
    """
    with scoped_session() as session:
        return session.query(Users).filter_by(username=username).first()


def get_all_users() -> List[dict]:
    """
    Function that allow to get_all_users
    :return: all users
    """
    with scoped_session() as session:
        return session.query(Users).all()


def registation_user(email: str, username: str, password: int) -> USERS_TYPE:
    """
    Function that allow to register users
    :param email: users email
    :param password: users passwords
    :param username: usernames
    :return: dict with registered user
    """
    with scoped_session()as session:
        user = Users(email=email, username=username)
        user.set_password(password)
        session.add(user)
    return user


def update_user(users_id: int, user_data: dict) -> USERS_TYPE:
    """
    Function that allow to update user
    :param users_id: users id
    :param user_data: fields that should be updated
    :return: dict with updated user data
    """
    with scoped_session() as session:
        user = session.query(Users).filter_by(users_id=users_id).one()
        for k, v in user_data.items():
            setattr(user, k, v)
        return user


def update_password(users_id: int, password: int) -> dict:
    """
    Function that allow to update password
    :param users_id: user id
    :param password: password
    :return: updated password
    """
    with scoped_session() as session:
        new_password = generate_password_hash(password)
        return session.query(Users).filter_by(users_id=users_id).update({"password_hash": new_password})


def delete_user(users_id: int):
    """
    Function that allow to delete user
    :param users_id: user id
    :return: delete user
    """
    with scoped_session() as session:
        session.query(Users).filter_by(users_id=users_id).delete()
        return 204


def delete_all_users():
    """
    Function that allow to delete all users
    :return: deleted users
    """
    with scoped_session() as session:
        session.query(Users).delete()
        return 204

#
# class UserSchema(Schema):
#     """
#     Class UserSchema enables us to easily sanitize and validate content according to a schema
#     """
#     re_email = r"(^[A-Za-z0-9_.+-]+@[A-Za-z0-9]+\.[A-Za-z]+$)"
#     re_password = r"^[A-Za-z0-9]*$"
#     users_id = fields.Integer()
#     username = fields.String(required=True)
#     email = fields.Email(required=True, validate=validate.Regexp(regex=re_email, flags=0,
#                                                                  error="Email must include only letters numbers and "
#                                                                        "special symbols"))
#     password_hash = fields.String(required=True, validate=validate.Regexp(regex=re_password, flags=0,
#                                                                           error='Password must include '
#                                                                                 'only letters and numbers'))
#     password = fields.String()

# class UserBaseSchema(Schema):
#     re_email = r"(^[A-Za-z0-9_.+-]+@[A-Za-z0-9]+\.[A-Za-z]+$)"
#     re_password = r"^[A-Za-z0-9]{8,}$"
#     users_id = fields.Integer()
#     username = fields.String(required=True)
#     email = fields.Email(required=True, validate=validate.Regexp(regex=re_email, flags=0,
#                                                                  error="Email must include only letters numbers and "
#                                                                        "special symbols"))


# class UserOutputSchema(UserBaseSchema):
#     password_hash = fields.String(required=True, validate=validate.Regexp(regex=super().re_password, flags=0,
#                                                                           error="Password must include letters and "
#                                                                                 "numbers or password length is less "
#                                                                                 "then 8 symbols "))
