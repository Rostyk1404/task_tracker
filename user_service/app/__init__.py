"""
Initialization of app
"""
from flask import Flask
from flask_restful import Api
from flask_jwt_extended import JWTManager
from app_config import Config


app = Flask(__name__)
api = Api(app)
app.config.from_object(Config)
app.config['JWT_SECRET_KEY'] = 'Dude'
# JwtManager object
jwt = JWTManager(app)
from app.routes import user_routes
from app.routes import tasks_routes
