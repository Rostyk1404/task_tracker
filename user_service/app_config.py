"""
This is a config module for user_service
"""
import os
from postgres_helper.postgres_config import HOST, PORT, POSTGRES_PASSWORD, POSTGRES_USER, USER_DB

BASEDIR = os.path.abspath(os.path.dirname(__file__))


class Config:
    """
    Configuration object. Contains database URI, and run mode(debug, production or testing)
    """
    DATABASE_URL = f"postgresql://{POSTGRES_USER}:{POSTGRES_PASSWORD}@{HOST}:{PORT}/{USER_DB}"
