"""
Schemas classes
"""
from marshmallow import Schema, fields, validate


class UserSchema(Schema):
    """
    Data validation with library marshmallow
    """
    re_email = r"(^[A-Za-z0-9_.+-]+@[A-Za-z0-9]+\.[A-Za-z]+$)"
    re_password = r"^(?=.*[A-Za-z])(?=.*\d)[A-Za-z\d]{8,}$"
    users_id = fields.Integer()
    username = fields.String(required=True)
    email = fields.Email(required=True, validate=validate.Regexp(regex=re_email, flags=0,
                                                                 error="Email must include"
                                                                       "only letters numbers and special symbols"))
    password_hash = fields.String(required=True, validate=validate.Regexp(regex=re_password, flags=0,
                                                                          error="Password must include letters and "
                                                                                "numbers or password length is less "
                                                                                "then 8 symbols "))
    password = fields.String()


class TaskSchema(Schema):
    """
    Class TaskSchema to deserialize an object
    """
    task_id = fields.Integer(autoincrement=True, required=True)
    task_name = fields.String(required=True)
    user_id = fields.Integer(required=True)
